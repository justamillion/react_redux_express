const express    = require('express')
const app        = express()
const bodyParser = require('body-parser')
const logger     = require('morgan')
const port       = process.env.PORT || 3040
app.set('view engine', 'ejs');

app.use('/static',express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(logger('dev'))
require('./server/routes')(app);

module.exports = app;
