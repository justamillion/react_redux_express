import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import { Provider } from "react-redux"
import Bootstrap from './vendor/bootstrap-without-jquery';
import store from "./store"

import Layout   from "./components/Layout";

const app = document.getElementById('app');


ReactDOM.render(
    <Provider store={store}>
        <Router history={hashHistory}>
            <Route path="/" component={Layout}>
                
            </Route>
        </Router>
    </Provider>,
    app
);


