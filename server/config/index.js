const fs        = require('fs');
const path      = require('path');

const default_path = `${__dirname}/config.json`;
const local_path   = `${__dirname}/local.json`;
const has_local    = fs.existsSync(local_path);

if (!has_local){
 console.log('You souhld create the \'' + local_path +
                '\' file to avoid database credential pushed in git');
}

module.exports = has_local ? require(local_path) : require(default_path);
