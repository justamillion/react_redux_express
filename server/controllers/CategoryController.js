const Category = require('../models').category;
const Type =  require('../models').type;
const Item =  require('../models').item;

module.exports = {

    create : (req, res) =>  {
        return Category
        .create({
                name: req.body.name,
        })
        .then(created => res.status(201).send(created))
        .catch(error => res.status(400).send(error));
    },



    list : (req, res) =>  {

        return Category
        .findAll({
                include: [
                    {
                        model: Type,
                        as: 'type'
                    },{
                        model: Item,
                        as: 'items'
                    }

                ],
        })
        .then(categories => res.status(200).send(categories))
        .catch(error => res.status(400).send(error));
    },

};
