const Category = require('../models').category;
const Item =  require('../models').item;

module.exports = {

    create : (req, res) =>  {
        return Item
        .create({
                name: req.body.name,
        })
        .then(created => res.status(201).send(created))
        .catch(error => res.status(400).send(error));
    },



    list : (req, res) =>  {
        return Item
        .findAll({
                include: [{
                        model: Category,
                        as: 'category',
                }],
        })
        .then(items => res.status(200).send(items))
        .catch(error => res.status(400).send(error));
    },

};
