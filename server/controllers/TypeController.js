const Type = require('../models').type;

module.exports = {

    create : (req, res) =>  {
        return Type
        .create({
                name: req.body.name,
        })
        .then(type => res.status(201).send(type))
        .catch(error => res.status(400).send(error));
    },



    list : (req, res) =>  {
        return  Type.findAll()
        .then(types => res.status(200).send(types))
        .catch(error => res.status(400).send(error));
    },

};
