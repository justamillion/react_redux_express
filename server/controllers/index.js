const fs          = require('fs');
const path        = require('path');
const basename    = path.basename(module.filename);
const controllers = {};

// include all controllers in controllers object
// ex : controllers = {
//        CategoryController : { list : () => ..., create : () => ...},
//        TypeController : {  list : () => ..., create : () => ...}
//        ...
//      }
fs
.readdirSync(__dirname)
.filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
.forEach((file) => {
    controllers[file.substr(0, file.length -3)] = require(path.join(__dirname, file));
});

// Define a common baseUrl for all controllers
// ( to avoid conflict with react routes )
controllers.baseUrl = '/common';

module.exports = controllers;
