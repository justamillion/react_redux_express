const path   = require('path');
const fs     = require('fs');
const models = require('../models');

models.type.findOne().then(type => {
        let has_type = !!type;
        if (has_type) {
            console.log('\nYour database(types) is not empty, aborted ...\n');
            return;
        }else{
            console.log('\nFixtures starting ...\n');
            start_fixtures('1st');
            start_fixtures('2nd');
        }
})
.catch(error => console.log('an error occured, fixtures aborted :\n\t_' + error));

/*********************************
 * ********** function ********************/

start_fixtures = (num) => models.type.create(
        {
            "name" : num+" type",
            "slug" : num+"-type"
        })
.then(type_created => {
        let category = models.category.create(
        {
            "name"   : num+" category",
            "slug"   : num+"-category",
            "typeId"   : type_created.get('id')
        });
        return category;
})
.then(category_created => {
        let item1 = models.item.create(
        {
            "name"       : num+" item",
            "slug"       : num+"-item",
            'token'      : num+'faketoken',
            "categoryId"   : category_created.get('id')
        });
        console.log(num + ' fixture completed !\n');
})
.catch(error => console.log(error));
