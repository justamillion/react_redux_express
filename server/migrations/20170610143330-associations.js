module.exports = {
    up:  (queryInterface, Sequelize) => {
        queryInterface.addColumn( 'item', 'categoryId', {
                type: Sequelize.INTEGER,
                onDelete: 'CASCADE',
                // references: {
                //     model: 'categories',
                //     key: 'id',
                //     as: 'categoryId',
                // },
        });
        queryInterface.addColumn( 'category', 'typeId', {
                type: Sequelize.INTEGER,
                onDelete: 'CASCADE',
                // references: {
                //     model: 'types',
                //     key: 'id',
                //     as: 'typeId',
                // },

        });
    },

    down:  (queryInterface, Sequelize) => {
        queryInterface.removeColumn('category','typeId');
        queryInterface.removeColumn('item','categoryId');
    }
};
