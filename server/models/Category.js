module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define('category', {
            name: {
                type : DataTypes.STRING,
                allowNull : false,
            },
            slug: {
                type : DataTypes.STRING,
            },
    }, {
        classMethods: {
            associate: (models) => {
                Category.belongsTo(models.type);
                Category.hasMany(models.item);
            }
        }
    });
    return Category;
};
