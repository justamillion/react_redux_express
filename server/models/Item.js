module.exports = (sequelize, DataTypes) => {
    const Item = sequelize.define('item', {
            name: {
                type : DataTypes.STRING,
                allowNull : false,
            },
            slug: {
                type : DataTypes.STRING
            },
            token: {
                type : DataTypes.STRING,
                allowNull : false,
            },
    },{
        classMethods: {
            associate: (models) => {
                Item.belongsTo(models.category)
            }
        }
    });
    return Item;
};
