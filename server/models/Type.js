module.exports = (sequelize, DataTypes) => {
    const Type = sequelize.define('type', {
            name: {
                type : DataTypes.STRING,
                allowNull : false,
            },
            slug: {
                type : DataTypes.STRING,
            },
    }, {
        classMethods: {
            associate: (models) => {

            }
        }
    });
    return Type;
};
