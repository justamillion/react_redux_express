const fs        = require('fs');
const path      = require('path');
const Sequelize = require('sequelize');
const basename  = path.basename(module.filename);
const env       = process.env.NODE_ENV || 'development';

const config    = require('../config')[env];
const db        = {};

let sequelize;
if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
    sequelize = new Sequelize(config.database, config.username, config.password, config);
}

//it s just a test of connection
sequelize.authenticate()
.then(() => {
    console.log('Connection to the database has been established successfully.');
})
.catch(err => {
    console.error('Unable to connect to the database:', err);
});

// setting default common options for define
sequelize.options.define.freezeTableName = true;
// sequelize.options.define.underscoredAll  = true;
// sequelize.options.define.underscored     = true;

//importing models
fs
.readdirSync(__dirname)
.filter((file) =>
(file.indexOf('.') !== 0) &&
(file !== basename) &&
(file.slice(-3) === '.js')).forEach((file) =>
{
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
});

Object.keys(db).forEach((modelName) => {
    if (db[modelName].options &&
        db[modelName].options.classMethods &&
        db[modelName].options.classMethods.associate) {
        db[modelName].options.classMethods.associate(db);
    }
});

db.sequelize   = sequelize;
db.Sequelize   = Sequelize;
module.exports = db;
