const controller = require('../controllers').CategoryController;
const baseUrl    = require('../controllers').baseUrl.concat('/category');


/**
 * Category routing
 *
 */
module.exports = (app) => {
    app.post(baseUrl, controller.create);
    app.get(baseUrl, controller.list);
    return app;
}
