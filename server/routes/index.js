const fs        = require('fs');
const path      = require('path');
const basename  = path.basename(module.filename);

/**
 * this is the main route file
 *
 */
 module.exports = (app) => {
     //include all route files
     fs
     .readdirSync(__dirname)
     .filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
     .forEach((file) => {
         app = require(path.join(__dirname, file))(app);
     });
     //all others routes (not mentionned above) will arrived here :
     app.get(
         '*',
         (req, res) => res.render('index')
     );
 };
