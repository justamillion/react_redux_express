const controller = require('../controllers').ItemController;
const baseUrl    = require('../controllers').baseUrl.concat('/item');


/**
 * Item routing
 *
 */
module.exports = (app) => {
    app.post(baseUrl, controller.create);
    app.get(baseUrl, controller.list);
    return app;
}
