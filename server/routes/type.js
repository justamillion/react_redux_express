const controller = require('../controllers').TypeController;
const baseUrl    = require('../controllers').baseUrl.concat('/type');


/**
 * Type Controller
 *
 */
module.exports = (app) => {
    app.post(baseUrl, controller.create);
    app.get(baseUrl, controller.list);
    return app;
}
